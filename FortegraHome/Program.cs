﻿using System;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace FortegraHome
{

    class Program
    {
        public static DateTime sStartDate;
        public static DateTime sEndDate;
        public static string sPaymentFile;
        public static string sSalesFile;
        public static string sCancelFile;

        static void Main(string[] args)
        {
            //string sMonthYear = "10/01/2021";         
            sEndDate =  DateTime.Today;
            sStartDate = DateTime.Today.AddDays(-7);
            //sEndDate = new DateTime(2021, 11, 1);
            //sStartDate = new DateTime(2021, 10, 1);
            FortegraSales fortegraSales = new FortegraSales();
            FortegraCancel fortegraCancel = new FortegraCancel();
            FortegraPayment fortegraPayment = new FortegraPayment();
            EMailFiles();

        }

        static void EMailFiles()
        {
            SmtpClient client = new SmtpClient("smtp.office365.com", 587);
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("srhanna@veritasglobal.com", "SilverSurfer3");
            client.EnableSsl = true;
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            if (sSalesFile.Length > 0)
            {
                System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(sSalesFile);
                mail.Attachments.Add(att);
            }
            if (sCancelFile.Length > 0)
            {
                System.Net.Mail.Attachment att2 = new System.Net.Mail.Attachment(sCancelFile);
                mail.Attachments.Add(att2);
            }
            if (sPaymentFile.Length > 0)
            {
                System.Net.Mail.Attachment att3 = new System.Net.Mail.Attachment(sPaymentFile);
                mail.Attachments.Add(att3);
            }
            mail.To.Add("cgonzalez@carguardadmin.com");
            mail.Bcc.Add("srhanna@carguardadmin.com");
            mail.Bcc.Add("dlambson@carguardadmin.com");
            mail.From = new MailAddress("srhanna@veritasglobal.com");
            mail.Subject = "Fortegra Weekly Sales and Cancellation";
            mail.IsBodyHtml = true;
            mail.Body = "";
            client.Send(mail);

        }

        
    }
}
