﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortegraHome
{
    class FortegraPayment
    {
        private static string sMonthYear;
        public FortegraPayment()
        {
            createFilePath();
            createPaymentFile();
        }
        private static void createFilePath()
        {
            string filePath = "c:/cga/fortegra/";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
        }
        private static string setPath()
        {
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return filePath;

        }
        private static string setMonth()
        {
            DateTime day = Convert.ToDateTime(sMonthYear);
            day = new DateTime(day.Year, day.Month, 1);
            day = day.AddDays(-1);
            return day.ToString("MMMM");
        }
        private static string setYear()
        {
            DateTime day = Convert.ToDateTime(sMonthYear);
            day = new DateTime(day.Year, day.Month, 1);
            day = day.AddDays(-1);
            return day.ToString("yyyy");
        }
        private static string setStart()
        {
            DateTime day = DateTime.Today;
            day = new DateTime(day.Year, day.Month, 1);
            day = day.AddDays(-1);
            day = new DateTime(day.Year, day.Month, 1);
            return day.ToString("dd/mm/yyyy");
        }
        private static string setEnd()
        {
            DateTime day = DateTime.Today;
            day = new DateTime(day.Year, day.Month, 1);
            day = day.AddDays(-1);
            return day.ToString("dd/mm/yyyy");
        }
        private static void createPaymentFile()
        {
            string sSQL = "select distinct '' as checkNo, '' as AccountNumber, c.FName +' '+ c.LName as customer, c.State as ContractOrginationState, " +
                "'' as agent,c.EffDate as effDate, c.ContractNo as PolicyNumber,d.DealerName as Admin, '' as insuranceCo, CustomerCost as ttlPrem, " +
                "ptc.NetCost as amount, '' as DESCRIPTION, '' as dwnPm, '' as ledgerBal, '' as discAm, ptc.NetCost as dlrCost, '' as pmtsMade " +
                "from Contract c " +
                "inner join Dealer d on d.DealerID = c.DealerID " +
                "inner join PlanTypeCoverage ptc on ptc.PlanTypeID = c.PlanTypeID and ptc.TermMonth = c.TermMonth " +
                "where c.datepaid < '" + Program.sEndDate + "' " +
                "and c.datepaid >= '" + Program.sStartDate + "' " +
                "";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(dBO.path());
            DataTable dt = dBO.dboInportInformation(sSQL);
            if (dt.Rows.Count == 0)
            {
                Program.sPaymentFile = "";
                return;
            }
            Program.sPaymentFile = "c:/cga/fortegra/Veritas_Payment" + Program.sEndDate.ToString("yyyyMMdd") + ".csv";
            StreamWriter sr = new StreamWriter("c:/cga/fortegra/Veritas_Payment" + Program.sEndDate.ToString("yyyyMMdd") + ".csv");            sr.WriteLine("CHECK NO,ACCOUNT #,CUSTOMER,CONTRACT ORIGINATION STATE,AGENT,EFF DATE,POLICY #,ADMIN,INSURANCE CO, TTL PREM , AMOUNT ,DESCRIPTION, DWN PMT , LEDGER BAL , DISC AMT , DLR COST ,PMTS MADE");
            dBO.dboClose();
            foreach (DataRow dr in dt.Rows)
            {
                string writeLine = "";
                writeLine = writeLine + dr["checkNo"].ToString().Replace(","," ") + ",";
                writeLine = writeLine + dr["AccountNumber"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["customer"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["ContractOrginationState"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["agent"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine +  Convert.ToDateTime(dr["effDate"]).ToString("MM/dd/yyyy").Replace(",", " ") + ",";
                writeLine = writeLine + dr["PolicyNumber"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["Admin"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["insuranceCo"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["ttlPrem"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["amount"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["DESCRIPTION"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["dwnPm"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["ledgerBal"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["discAm"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["dlrCost"].ToString().Replace(",", " ") + ",";
                writeLine = writeLine + dr["pmtsMade"].ToString().Replace(",", " ") + ",";
                sr.WriteLine(writeLine);
            }
            sr.Close();
        }
    }
}
