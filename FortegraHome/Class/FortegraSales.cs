﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace FortegraHome
{
    class FortegraSales
    {
        private static string sMonthYear;
        
        public FortegraSales()
        {
            //sMonthYear = sDate;
            set30Day();
            createFilePath();
            Premium();
        }
        private static void createFilePath()
        {
            string filePath = "c:/cga/fortegra/";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
        }
        private static void set30Day()
        {
            string sSQL = "update Contract set EffDate = dateadd(DAY, 30, SaleDate) where DATEDIFF(DAY, SaleDate, EffDate) != 30";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(dBO.path());
            dBO.dboAlterDBOUnsafe(sSQL);
            dBO.dboClose();
        }
        private static string setPath()
        {
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return filePath;

        }
        private static string setMonth()
        {            
            DateTime day = Convert.ToDateTime(sMonthYear);
            day = new DateTime(day.Year, day.Month,1);
            day = day.AddDays(-1);
            return day.ToString("MMMM");
        }
        private static string setYear()
        {
            DateTime day = Convert.ToDateTime(sMonthYear);
            day = new DateTime(day.Year, day.Month, 1);
            day = day.AddDays(-1);
            return day.ToString("yyyy");
        }
        private static string setStart(string sDate)
        {
            DateTime day = Convert.ToDateTime(sDate);
            day = new DateTime(day.Year, day.Month, 1);
            day = day.AddDays(-1);
            day = new DateTime(day.Year, day.Month, 1);
            return day.ToString("M/d/yyyy");
        }
        private static string setEnd(string sDate)
        {
            DateTime day = Convert.ToDateTime(sDate);
            day = new DateTime(day.Year, day.Month, 1);
            return day.ToString("M/d/yyyy");
        }
        private static void Premium()
        {
            string sSQL = "select ContractNo as InvoiceNumber,LName as lastName,FName as firstName, " +
                "c.Addr1 as Address1,case when c.Addr2 is null then '' else c.Addr2 end as Address2, c.City, c.State, c.Zip,c.Phone as phoneNumber, " +
                "'' as AccountNumber,SaleDate as purchaseDate,EffDate as deliveryDate,CustomerCost as PurchasPrice,'' as storeNumber,sku.SKU as SKU, " +
                "pt.PlanType as SKUDescription, 'Unknown' as Manuf,'Unknown' as Model,'Unknown' as SerialNumber,'1' as quantity,c.TermMonth as Term, " +
                "CustomerCost as WarrantyCost, (MoxyDealerCost-125) as dealerCost, '' as cancellation, '' as email, d.DealerNo as dealerNo, 'Y' as PaymentType, " +
                "'' as ESPPurchaseDate, '' as CellPhone " +
                "from Contract c " +
                "inner join Dealer d on c.DealerID = d.DealerID " +
                "inner join SKUPlan sku on sku.PlanTypeID = c.PlanTypeID and SKU.TermMonth = c.TermMonth " +
                "inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID " +
                "where c.datepaid < '"+ Program.sEndDate +"' " +
                "and c.datepaid >= '"+ Program.sStartDate +"' " +
                "and c.status = 'Paid' ";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(dBO.path());
            DataTable dt = dBO.dboInportInformation(sSQL);
            if (dt.Rows.Count == 0)
            {
                Program.sSalesFile = "";
                return;
            }
            Program.sSalesFile = "c:/cga/fortegra/Veritas_Sales_" + Program.sEndDate.ToString("yyyyMMdd") + ".csv";
            StreamWriter sr = new StreamWriter(Program.sSalesFile);
            sr.WriteLine("InvoiceNumber,lastName,firstName,Address1,Address2,City,State,Zip,phoneNumber,AccountNumber,purchaseDate,deliveryDate,PurchasPrice,storeNumber,SKU,SKUDescription,Manuf,Model,SerialNumber,quantity,Term,WarrantyCost,dealerCost,cancellation,email,dealerNo,PaymentType,ESPPurchaseDate,CellPhone");
            dBO.dboClose();
            foreach (DataRow dr in dt.Rows)
            {
                //string writeLine = "";
                //writeLine += dr["InvoiceNumber"].ToString() + ",";
                //writeLine += dr["lastName"].ToString().Replace(",", " ") + ",";
                //writeLine += dr["firstName"].ToString().Replace(",", " ") + ",";
                //writeLine += dr["Address1"].ToString().Replace(",", " ") + ",";
                //writeLine += dr["Address2"].ToString().Replace(",", " ") + ",";
                //writeLine += dr["City"].ToString() + ",";
                //writeLine += dr["State"].ToString() + ",";
                //writeLine += dr["Zip"].ToString() + ",";
                //writeLine += dr["phoneNumber"].ToString() + ",";
                //writeLine += dr["AccountNumber"].ToString() + ",";
                //writeLine += dr["purchaseDate"].ToString() + ",";
                //writeLine += dr["deliveryDate"].ToString() + ",";
                //writeLine += dr["PurchasPrice"].ToString() + ",";
                //writeLine += dr["storeNumber"].ToString() + ",";
                //writeLine += dr["SKU"].ToString().Replace(",", " ") + ",";
                //writeLine += dr["SKUDescription"].ToString().Replace(",", " ") + ",";
                //writeLine += dr["Manuf"].ToString() + ",";
                //writeLine += dr["Model"].ToString() + ",";
                //writeLine += dr["SerialNumber"].ToString() + ",";
                //writeLine += dr["quantity"].ToString() + ",";
                //writeLine += dr["Term"].ToString() + ",";
                //writeLine += dr["WarrantyCost"].ToString() + ",";
                //writeLine += dr["dealerCost"].ToString() + ",";
                //writeLine += dr["cancellation"].ToString() + ",";
                //writeLine += dr["email"].ToString() + ",";
                //writeLine += dr["dealerNo"].ToString() + ",";
                //writeLine += dr["PaymentType"].ToString() + ",";
                //writeLine += dr["ESPPurchaseDate"].ToString() + ",";
                //writeLine += dr["CellPhone"].ToString();
                //sr.WriteLine(writeLine);
                breakDown(dr["InvoiceNumber"].ToString());
                surcharges(dr["InvoiceNumber"].ToString());
            }
            sr.Close();
            void surcharges(string sContractNo)
            {
                sSQL = "select distinct ContractNo as InvoiceNumber,LName as lastName,FName as firstName, " +
                    "c.Addr1 as Address1,case when c.Addr2 is null then '' else c.Addr2 end as Address2, " +
                    "c.City, c.State, c.Zip,c.Phone as phoneNumber, '' as AccountNumber,SaleDate as purchaseDate, " +
                    "EffDate as deliveryDate,skus.NetCost as PurchasPrice,'' as storeNumber,skus.SKU as SKU, sg.Surcharge as SKUDescription, " +
                    "'Unknown' as Manuf,'Unknown' as Model,'Unknown' as SerialNumber,'1' as quantity, " +
                    "c.TermMonth as Term,skus.NetCost as WarrantyCost, skus.NetCost as dealerCost, " +
                    "'' as cancellation, '' as email, d.DealerNo as dealerNo, 'Y' as PaymentType, " +
                    "'' as ESPPurchaseDate, '' as CellPhone " +
                    "from Contract c " +
                    "inner join Dealer d on c.DealerID = d.DealerID " +
                    "inner join ContractSurcharge cs on cs.ContractID = c.ContractID " +
                    "inner join SKUSurcharge skus on skus.SurchargeID = cs.SurchargeID and c.TermMonth = skus.TermMonth and c.PlanTypeID = skus.PlanTypeID " +
                    "inner join Surcharge sg on sg.SurchargeID = cs.SurchargeID " +
                    "where ContractNo = '"+sContractNo+"'";
                dBO.dboOpen(dBO.path());
                DataTable dt2 = dBO.dboInportInformation(sSQL);
                dBO.dboClose();
                int iCounter = 1;
                int iCounter2 = 0;
                foreach (DataRow dr in dt2.Rows)
                {
                    string writeLine = "";
                    writeLine += dr["InvoiceNumber"].ToString()+"-"+iCounter2+""+iCounter + ",";
                    writeLine += dr["lastName"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["firstName"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["Address1"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["Address2"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["City"].ToString() + ",";
                    writeLine += dr["State"].ToString() + ",";
                    writeLine += dr["Zip"].ToString() + ",";
                    writeLine += dr["phoneNumber"].ToString() + ",";
                    writeLine += dr["AccountNumber"].ToString() + ",";
                    writeLine += dr["purchaseDate"].ToString() + ",";
                    writeLine += dr["deliveryDate"].ToString() + ",";
                    writeLine += dr["PurchasPrice"].ToString() + ",";
                    writeLine += dr["storeNumber"].ToString() + ",";
                    writeLine += dr["SKU"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["SKUDescription"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["Manuf"].ToString() + ",";
                    writeLine += dr["Model"].ToString() + ",";
                    writeLine += dr["SerialNumber"].ToString() + ",";
                    writeLine += dr["quantity"].ToString() + ",";
                    writeLine += dr["Term"].ToString() + ",";
                    writeLine += dr["WarrantyCost"].ToString() + ",";
                    writeLine += dr["dealerCost"].ToString() + ",";
                    writeLine += dr["cancellation"].ToString() + ",";
                    writeLine += dr["email"].ToString() + ",";
                    writeLine += dr["dealerNo"].ToString() + ",";
                    writeLine += dr["PaymentType"].ToString() + ",";
                    writeLine += dr["ESPPurchaseDate"].ToString() + ",";
                    writeLine += dr["CellPhone"].ToString();
                    writeLine = writeLine.Replace("\n", "");
                    writeLine = writeLine.Replace("\r", "");
                    sr.WriteLine(writeLine);
                    iCounter++;
                    if (iCounter == 10)
                    {
                        iCounter = 0;
                        iCounter2++;
                    }
                }
            }
            void breakDown(string sContractNo)
            {
                sSQL = "select ContractNo as InvoiceNumber,LName as lastName,FName as firstName, " +
                    "c.Addr1 as Address1, case when c.Addr2 is null then '' else c.Addr2 end as Address2, c.City, c.State, c.Zip,c.Phone as phoneNumber,  " +
                    "'' as AccountNumber,SaleDate as purchaseDate,EffDate as deliveryDate,ptc.Price as PurchasPrice," +
                    "' ' as storeNumber,ptc.SKU as SKU, ptc.Coverage as SKUDescription, 'Unknown' as Manuf,'Unknown' as Model,'Unknown' as SerialNumber, " +
                    "'1' as quantity,c.TermMonth as Term, CustomerCost as WarrantyCost, ptc.NetCost as dealerCost, '' as cancellation,  " +
                    "'' as email, d.DealerNo as dealerNo, 'Y' as PaymentType, '' as ESPPurchaseDate, '' as CellPhone " +
                    "from Contract c " +
                    "inner join Dealer d on c.DealerID = d.DealerID " +
                    "inner join PlanTypeCoverage ptc on ptc.PlanTypeID = c.PlanTypeID and c.TermMonth = ptc.TermMonth " +
                    "inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID " +
                    "where c.contractno = '"+sContractNo+"'" +
                    " order by ContractNo";

                dBO.dboOpen(dBO.path());
                DataTable dt3 = dBO.dboInportInformation(sSQL);
                dBO.dboClose();
                foreach (DataRow dr in dt3.Rows)
                {
                    string writeLine = "";
                    writeLine += dr["InvoiceNumber"].ToString() + ",";
                    writeLine += dr["lastName"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["firstName"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["Address1"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["Address2"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["City"].ToString() + ",";
                    writeLine += dr["State"].ToString() + ",";
                    writeLine += dr["Zip"].ToString() + ",";
                    writeLine += dr["phoneNumber"].ToString() + ",";
                    writeLine += dr["AccountNumber"].ToString() + ",";
                    writeLine += dr["purchaseDate"].ToString() + ",";
                    writeLine += dr["deliveryDate"].ToString() + ",";
                    writeLine += dr["PurchasPrice"].ToString() + ",";
                    writeLine += dr["storeNumber"].ToString() + ",";
                    writeLine += dr["SKU"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["SKUDescription"].ToString().Replace(",", " ") + ",";
                    writeLine += dr["Manuf"].ToString() + ",";
                    writeLine += dr["Model"].ToString() + ",";
                    writeLine += dr["SerialNumber"].ToString() + ",";
                    writeLine += dr["quantity"].ToString() + ",";
                    writeLine += dr["Term"].ToString() + ",";
                    writeLine += dr["WarrantyCost"].ToString() + ",";
                    writeLine += dr["dealerCost"].ToString() + ",";
                    writeLine += dr["cancellation"].ToString() + ",";
                    writeLine += dr["email"].ToString() + ",";
                    writeLine += dr["dealerNo"].ToString() + ",";
                    writeLine += dr["PaymentType"].ToString() + ",";
                    writeLine += dr["ESPPurchaseDate"].ToString() + ",";
                    writeLine += dr["CellPhone"].ToString();
                    writeLine = writeLine.Replace("\n","");
                    writeLine = writeLine.Replace("\r", "");
                    sr.WriteLine(writeLine);
                }
            }
        }
    }
}
